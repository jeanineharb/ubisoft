# Part 1

Given customerid = 33948, here is the proposed query:

```sql
SELECT 
	*, 
	COUNT(DISTINCT ip_address) OVER (PARTITION BY customerid) as count_ip, 
	SUM(amount_eur) OVER (ORDER BY order_date rows between unbounded preceding and current row) AS sum_eur
FROM 
	`154c3c10d4ec4e768de48b6206eb5862` 
WHERE customerid = 33948
```





