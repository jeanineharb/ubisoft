# Part 3

Given customerid = 33948, we add the following rows for testing purposes :

```sql
INSERT INTO table 154C3C10D4EC4E768DE48B6206EB5862 values 
(33948,'2019-10-04','2019-10-04 11:26:01','25 rue Lavoisier','12.77.49.61',50),
(33948,'2019-10-04','2019-10-04 11:29:01','25 rue Lavoisier','12.77.49.61',10);
```

Here is the proposed query :
```sql
SELECT  
	* , 
	SUM(amount_eur) OVER 
		(PARTITION BY FLOOR ((unix_timestamp(order_datetime)+599)/600) * 600 rows between 1 following and unbounded following) 
FROM `154c3c10d4ec4e768de48b6206eb5862`
WHERE customerid = 33948 
SORT BY order_datetime;
```
