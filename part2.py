# Part 2 - Spark

from pyspark.sql.types import IntegerType, BooleanType
from pyspark.sql.functions import udf

def count_distinct_chars(s):
  return len(set(s.strip(" ")))

def is_paris(s):
    return True if "rue de paris" in s.lower() else False

count_distinct_udf = udf(count_distinct_chars, IntegerType())
is_paris_udf = udf(is_paris, BooleanType())

df = schemaTransaction.withColumn(
	"len_address", count_distinct_udf(schemaTransaction.address)
	).withColumn("is_paris", is_paris_udf(schemaTransaction.address))

df.show(n=2)